# Joker

Joker es una app para la prueba de SquadMakers.

# Owner
- Valentin Castillo Contreras

# New Features!

  - App para obtener Bromas
  - App para procesos matematicos sencillos
  - Archivo ANSWERS contiene la segunda parte de la entrega


### Requeriments

 - Python 3.8

### Installation

Instalaciones de dependencias y devDependencias para empezar.
Para la instalación, tu debes clonar el siguiente repositorio:

* [Repositorio](https://gitlab.com/valentinc94/joker)

Nota: Deber tener en cuenta, antes de correr el proyecto crear el archivo .env y que contenga esto
```sh
DJANGO_SECRET_KEY=2mlo*gaorp+j-0$8)8$=!hfcvp-xp_&8hb3xwrv$ao*r$ijb3!
```

#### Para el ambiente de desarrollo

- **Contenedor docker:**
```sh
$ git clone <repositorio>
$ docker-compose build
$ docker-compose up
```
*Nota: verificar que se ejecuten todas las migraciones (probablemente se deba correr docker-compose up nuevamente).*

```sh
# Para crear super usuario
$ docker-compose run joker python manage.py createsuperuser
```
```sh
# Para correr el shell de django
$ docker-compose run joker python manage.py shell
```

Para entrar en la consola del contenedor, en otra terminal, ejecutar:
```sh
# Tiene que estar corriendo el contenedor para poder ejecutar
$ docker-compose exec monet bash
```

```sh
# Para ejecutar los test unitarios
$ docker-compose run joker python manage.py test
```


Para interactuar con los endpoints se necesita un Api Key puede ser generada desde el backoffice:
```sh
$ url_backoffice = http://0.0.0.0:8000/
$ url_backoffice_generate_new_api_key = http://0.0.0.0:8000/admin/rest_framework_api_key/apikey/
```

# Uso API KEY
```sh
$ headers = {"x-api-key": "V1y42BP1.ywqstdaic0Ci8jo68X74xafQtN79EnTt"}
```

# Url Documentacion Endpoints
```sh
$ url_docs_swagger = http://0.0.0.0:8000/docs/
```

# fixtures Datos de prueba
```sh
$ sudo docker-compose run joker python manage.py loaddata joker/fixtures/fixture.json
```
