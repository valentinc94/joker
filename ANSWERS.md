# Que repositorio utilizarias?

### Respuesta

 - PostgreSQL: Como es un modelo de una sola tabla en realidad no importa mucho si elijo base de dato relacional o no ya que no hay relaciones,
 - En este caso se uso postgreSQL por la rapidez de terminar la tarea

### Tabla
- Prank: Fields(uuid, provider, provider_reference, joke, created_at, modified_at)

### Fields of Prank Model
- uuid
- provider: proveedor  = ["OTRO", "CHUCK_NORRIS", "DAD"]
- provider_reference: id en el proveedor
- joke: Broma
- created: fecha de creacion
- updated: fecha de actualizacion
