from django.urls import resolve
from parameterized import parameterized
from rest_framework_api_key.models import APIKey
from test_plus import APITestCase

from joker.apps.jokes import constants, factories
from joker.interfaces.api.jokes import views
from joker.utils import test_case


class JokesTestBaseCase(APITestCase):
    def setUp(self) -> None:
        super().setUp()
        self.api_key, self.key = APIKey.objects.create_key(name="SUPER_KEY")
        self.url = self.reverse(name="jokes_api:create_joke")
        self.headers = {"HTTP_X_API_KEY": self.key, "format": "json"}


class CreateJokeMethodGetTestCase(JokesTestBaseCase):
    def test_create_joke_api_view_resolves_correctly(self):
        found = resolve(self.url)
        self.assertTrue(found.func.__name__, views.CreateJoke.__name__)

    @parameterized.expand(
        [
            (constants.JokesSources.CHUCK_NORRIS,),
            (constants.JokesSources.DAD,),
        ]
    )
    def test_get_joke_api_view_return_ok(self, provider: constants.JokesSources):
        url = f"{self.url}?provider={provider}"
        response = self.get(url_name=url, extra=self.headers)

        self.assert_http_200_ok(response)
        result, code_transaction, _, _ = test_case.process_response(response)

        self.assertEqual(code_transaction, "OK")
        self.assertEqual(result["data"]["provider"], provider)
        self.assertIsNotNone(result["data"]["joke"])

    def test_get_joke_api_view_return_raises_exceptions(self):
        response = self.get(url_name=self.url, extra=self.headers)

        self.assert_http_404_not_found(response)
        _, code_transaction, _, message = test_case.process_response(response)

        self.assertEqual(code_transaction, "JOKE_DOES_NOT_EXIST")
        self.assertEqual(message, "No existe registro del proveedor de Broma.")


class CreateJokeMethodPostTestCase(JokesTestBaseCase):
    @parameterized.expand(
        [
            ("Oye, ¿cuál es tu plato favorito y por qué?. Pues el hondo, porque cabe más comida…",),
            ("¿Tienes WiFi? Sí. ¿Y cuál es la clave? Tener dinero y pagarlo.",),
        ]
    )
    def test_post_joke_api_view_return_ok(self, joke: str):
        response = self.post(url_name=self.url, data={"joke": joke}, extra=self.headers)
        result, code_transaction, _, _ = test_case.process_response(response)
        self.assert_http_200_ok(response)
        result, code_transaction, _, _ = test_case.process_response(response)

        self.assertEqual(code_transaction, "OK")
        self.assertEqual(result["data"]["provider"], constants.JokesSources.OTHER)
        self.assertEqual(result["data"]["joke"], joke)

    def test_post_joke_api_view_return_raises_exceptions_joke_already_exists(self):
        joke = "¿Tienes WiFi? Sí. ¿Y cuál es la clave? Tener dinero y pagarlo."

        factories.PrankFactory(joke=joke)
        response = self.post(url_name=self.url, data={"joke": joke}, extra=self.headers)

        self.assert_http_400_bad_request(response)
        _, code_transaction, _, message = test_case.process_response(response)

        self.assertEqual(code_transaction, "JOKE_ALREADY_EXIST")
        self.assertEqual(message, "Ya existe registro de la broma.")


class CreateJokeMethodPutTestCase(JokesTestBaseCase):
    def test_put_joke_api_view_return_ok(self):
        prank = factories.PrankFactory(provider=constants.JokesSources.DAD)

        url = f"{self.url}?prank_id={prank.id}"

        response = self.put(
            url_name=url,
            data={"joke": "otra broma"},
            extra=self.headers,
        )
        result, code_transaction, _, _ = test_case.process_response(response)
        self.assert_http_200_ok(response)
        result, code_transaction, data, _ = test_case.process_response(response)

        prank.refresh_from_db()

        self.assertEqual(code_transaction, "OK")
        self.assertEqual(data["joke"], "otra broma")

    def test_put_joke_api_view_return_raises_exceptions_joke_already_exists(self):
        response = self.put(
            url_name=self.url,
            data={"joke": "otra broma"},
            extra=self.headers,
        )

        self.assert_http_404_not_found(response)
        result, code_transaction, _, message = test_case.process_response(response)
        self.assertEqual(code_transaction, "JOKE_DOES_NOT_EXIST")
        self.assertEqual(message, "No existe registro de la broma.")


class CreateJokeMethodDeleteTestCase(JokesTestBaseCase):
    def test_delete_joke_api_view_return_ok(self):
        prank = factories.PrankFactory()

        url = f"{self.url}?prank_id={prank.id}"
        response = self.delete(
            url_name=url,
            data={},
            extra=self.headers,
        )
        self.assert_http_200_ok(response)

        _, code_transaction, _, message = test_case.process_response(response)

        self.assertEqual(code_transaction, "OK")
        self.assertEqual(message, "Broma eliminada Exitosamente.")

    def test_delete_joke_api_view_return_raises_exceptions_joke_already_exists(self):
        response = self.delete(
            url_name=self.url,
            data={},
            extra=self.headers,
        )

        self.assert_http_404_not_found(response)
        result, code_transaction, _, message = test_case.process_response(response)
        self.assertEqual(code_transaction, "JOKE_DOES_NOT_EXIST")
        self.assertEqual(message, "No existe registro de la broma.")
