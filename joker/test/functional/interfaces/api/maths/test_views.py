from typing import Any

from django.urls import resolve
from parameterized import parameterized
from rest_framework_api_key.models import APIKey
from test_plus import APITestCase

from joker.interfaces.api.maths import views
from joker.utils import test_case


class LowestCommonMultipleTestCase(APITestCase):
    def setUp(self) -> None:
        super().setUp()
        self.api_key, self.key = APIKey.objects.create_key(name="SUPER_KEY")
        self.url = self.reverse(name="maths_api:get_lowest_common_multiple")
        self.headers = {"HTTP_X_API_KEY": self.key, "format": "json"}

    def test_lowest_common_multiple_api_view_resolves_correctly(self):
        found = resolve(self.url)
        self.assertTrue(found.func.__name__, views.LowestCommonMultiple.__name__)

    def test_lowest_common_multiple_api_view_return_ok(self):
        numbers = [27, 51]
        url = f"{self.url}?numbers={numbers}"
        response = self.get(url_name=url, extra=self.headers)

        self.assert_http_200_ok(response)
        result, code_transaction, _, _ = test_case.process_response(response)

        self.assertEqual(code_transaction, "OK")
        self.assertEqual(result["lowest_common_multiple"], 459)

    @parameterized.expand(
        [
            (
                "hello_world",
                "INVALID_INPUT_EXIST",
                "Lista invalida, porfavor ingresar una lista de Numeros enteros.",
            ),
            (
                1,
                "INVALID_INPUT_EXIST",
                "Lista invalida, porfavor ingresar una lista de Numeros enteros.",
            ),
            (
                ["1", "hello_world"],
                "INVALID_INPUT_EXIST",
                "Lista invalida, porfavor ingresar una lista de Numeros enteros.",
            ),
            (
                True,
                "INVALID_INPUT_EXIST",
                "Lista invalida, porfavor ingresar una lista de Numeros enteros.",
            ),
            (
                {"key": "value"},
                "INVALID_INPUT_EXIST",
                "Lista invalida, porfavor ingresar una lista de Numeros enteros.",
            ),
        ]
    )
    def test_lowest_common_multiple_raises_exceptions(
        self, numbers: Any, code_transaction_expected: str, message_expected
    ):
        url = f"{self.url}?numbers={numbers}"
        response = self.get(url_name=url, extra=self.headers)

        self.assert_http_400_bad_request(response)
        _, code_transaction, _, message = test_case.process_response(response)

        self.assertEqual(code_transaction, code_transaction_expected)
        self.assertEqual(message, message_expected)


class NextNumberTestCase(APITestCase):
    def setUp(self) -> None:
        super().setUp()
        self.api_key, self.key = APIKey.objects.create_key(name="SUPER_KEY")
        self.url = self.reverse(name="maths_api:get_next_sequence")
        self.headers = {"HTTP_X_API_KEY": self.key, "format": "json"}

    def test_next_number_api_view_resolves_correctly(self):
        found = resolve(self.url)
        self.assertTrue(found.func.__name__, views.NextNumber.__name__)

    def test_next_number_api_view_return_ok(self):
        number = 25
        url = f"{self.url}?number={number}"
        response = self.get(url_name=url, extra=self.headers)

        self.assert_http_200_ok(response)
        result, code_transaction, _, _ = test_case.process_response(response)

        self.assertEqual(code_transaction, "OK")
        self.assertEqual(result["next_number"], number + 1)

    @parameterized.expand(
        [
            (
                "hello_world",
                "INVALID_INPUT_EXIST",
                "Numero invalido, porfavor ingresar un Numero entero.",
            ),
            (
                [1],
                "INVALID_INPUT_EXIST",
                "Numero invalido, porfavor ingresar un Numero entero.",
            ),
            (
                ["1", "hello_world"],
                "INVALID_INPUT_EXIST",
                "Numero invalido, porfavor ingresar un Numero entero.",
            ),
            (
                True,
                "INVALID_INPUT_EXIST",
                "Numero invalido, porfavor ingresar un Numero entero.",
            ),
            (
                {"key": "value"},
                "INVALID_INPUT_EXIST",
                "Numero invalido, porfavor ingresar un Numero entero.",
            ),
        ]
    )
    def test_lowest_common_multiple_raises_exceptions(
        self, number: Any, code_transaction_expected: str, message_expected
    ):
        url = f"{self.url}?number={number}"
        response = self.get(url_name=url, extra=self.headers)

        self.assert_http_400_bad_request(response)
        _, code_transaction, _, message = test_case.process_response(response)

        self.assertEqual(code_transaction, code_transaction_expected)
        self.assertEqual(message, message_expected)
