from django.urls import include, path

urlpatterns = [
    path("", include("joker.interfaces.api.jokes.urls")),
    path("", include("joker.interfaces.api.maths.urls")),
]
