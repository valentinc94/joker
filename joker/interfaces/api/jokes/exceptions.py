from joker.interfaces.api.commons import exceptions


class JokeAlreadyExistsAPIException(exceptions.BaseAPIException):
    status_code = 400
    default_detail = "Ya existe registro de la broma."
    default_code = "JOKE_ALREADY_EXIST"


class JokeDoesNotExistsAPIException(exceptions.BaseAPIException):
    status_code = 404
    default_detail = "No existe registro de la broma."
    default_code = "JOKE_DOES_NOT_EXIST"


class NotMatchJokeProviderAPIException(exceptions.BaseAPIException):
    status_code = 404
    default_detail = "No existe registro del proveedor de Broma."
    default_code = "JOKE_DOES_NOT_EXIST"


class UnableToGetJokeAPIException(exceptions.BaseAPIException):
    status_code = 400
    default_detail = "No es posible obtener la broma del proveedor."
    default_code = "UNABLE_TO_GET_JOKE_BY_PROVIDER"
