from django.urls import path

from joker.interfaces.api.jokes import views

app_name = "jokes_api"


urlpatterns = [
    path(
        "api/v1/jokes/",
        views.CreateJoke.as_view(),
        name="create_joke",
    ),
]
