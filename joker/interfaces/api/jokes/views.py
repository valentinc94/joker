from drf_yasg2 import openapi
from drf_yasg2.utils import swagger_auto_schema
from rest_framework import status, views
from rest_framework.response import Response

from joker.apps.jokes import constants, models
from joker.domains.jokes import operations, processes
from joker.interfaces.api.commons import exceptions as commons_exceptions
from joker.interfaces.api.commons import serializers as commons_serializers
from joker.interfaces.api.jokes import exceptions, serializers
from joker.utils import mixins


class CreateJoke(mixins.APIWithUserPermissionsMixin, views.APIView):
    serializer_class = serializers.PrankSerializer

    @swagger_auto_schema(
        operation_description="Endpoint para Generacion de Broma aleatoria",
        manual_parameters=[
            openapi.Parameter(
                "provider",
                in_=openapi.IN_QUERY,
                description=f"Nombre del proveedor: {constants.JokesSources.CHUCK_NORRIS}, {constants.JokesSources.DAD}",
                type=openapi.TYPE_STRING,
                required=True,
            )
        ],
        responses={
            200: serializers.PrankSerializer(many=False),
            400: openapi.Response(
                type=openapi.TYPE_OBJECT,
                description="",
                schema=commons_serializers.ExceptionSerializer(many=False),
                examples={
                    "application/json": exceptions.JokeAlreadyExistsAPIException().get_full_details()
                },
            ),
            401: openapi.Response(
                type=openapi.TYPE_OBJECT,
                description="",
                schema=commons_serializers.ExceptionSerializer(many=False),
                examples={
                    "application/json": commons_exceptions.UserUnauthorizedAPIException().get_full_details()
                },
            ),
        },
    )
    def get(self, request):
        provider = request.GET.get("provider", None)

        try:
            joke = processes.generate_random_joke(provider=provider)
        except (processes.ProviderDoesNotExist, processes.NotMatchJokeProvider):
            raise exceptions.NotMatchJokeProviderAPIException()
        except operations.FailedObtainingJoke:
            raise exceptions.UnableToGetJokeAPIException()

        serializer = serializers.PrankSerializer(data=joke)
        serializer.is_valid(raise_exception=True)
        prank = serializer.create(serializer.validated_data)
        serializer_data = serializers.PrankDetailSerializer(prank)
        return Response(data=serializer_data.data, status=status.HTTP_201_CREATED)

    @swagger_auto_schema(
        operation_description="Endpoint para Registro de Broma",
        request_body=commons_serializers.ExceptionSerializer(many=False),
        responses={
            200: serializers.PrankSerializer(many=False),
            400: openapi.Response(
                type=openapi.TYPE_OBJECT,
                description="",
                schema=commons_serializers.ExceptionSerializer(many=False),
                examples={
                    "application/json": exceptions.JokeAlreadyExistsAPIException().get_full_details()
                },
            ),
            401: openapi.Response(
                type=openapi.TYPE_OBJECT,
                description="",
                schema=commons_serializers.ExceptionSerializer(many=False),
                examples={
                    "application/json": commons_exceptions.UserUnauthorizedAPIException().get_full_details()
                },
            ),
        },
    )
    def post(self, request):
        serializer = serializers.PrankSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        prank = serializer.create(serializer.validated_data)
        serializer_data = serializers.PrankDetailSerializer(prank)
        return Response(data=serializer_data.data, status=status.HTTP_201_CREATED)

    @swagger_auto_schema(
        operation_description="Endpoint para Actualizar una Broma",
        manual_parameters=[
            openapi.Parameter(
                "prank_id",
                in_=openapi.IN_QUERY,
                description=f"Id de la Broma",
                type=openapi.TYPE_STRING,
                required=True,
            )
        ],
        request_body=commons_serializers.ExceptionSerializer(many=False),
        responses={
            200: serializers.PrankSerializer(many=False),
            400: openapi.Response(
                type=openapi.TYPE_OBJECT,
                description="",
                schema=commons_serializers.ExceptionSerializer(many=False),
                examples={
                    "application/json": exceptions.JokeAlreadyExistsAPIException().get_full_details()
                },
            ),
            401: openapi.Response(
                type=openapi.TYPE_OBJECT,
                description="",
                schema=commons_serializers.ExceptionSerializer(many=False),
                examples={
                    "application/json": commons_exceptions.UserUnauthorizedAPIException().get_full_details()
                },
            ),
        },
    )
    def put(self, request):
        prank_id = request.GET.get("prank_id", None)

        try:
            prank = models.Prank.objects.get(id=prank_id)
        except models.Prank.DoesNotExist:
            raise exceptions.JokeDoesNotExistsAPIException()

        serializer = serializers.PrankSerializer(
            data=request.data,
            instance=prank,
            partial=True,
        )
        serializer.is_valid(raise_exception=True)
        prank = serializer.save()
        serializer_data = serializers.PrankDetailSerializer(prank)
        return Response(data=serializer_data.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_description="Endpoint para Eliminar una Broma",
        manual_parameters=[
            openapi.Parameter(
                "prank_id",
                in_=openapi.IN_QUERY,
                description=f"Id de la Broma",
                type=openapi.TYPE_STRING,
                required=True,
            )
        ],
        request_body=commons_serializers.ExceptionSerializer(many=False),
        responses={
            200: serializers.PrankSerializer(many=False),
            400: openapi.Response(
                type=openapi.TYPE_OBJECT,
                description="",
                schema=commons_serializers.ExceptionSerializer(many=False),
                examples={
                    "application/json": exceptions.JokeAlreadyExistsAPIException().get_full_details()
                },
            ),
            401: openapi.Response(
                type=openapi.TYPE_OBJECT,
                description="",
                schema=commons_serializers.ExceptionSerializer(many=False),
                examples={
                    "application/json": commons_exceptions.UserUnauthorizedAPIException().get_full_details()
                },
            ),
        },
    )
    def delete(self, request):
        prank_id = request.GET.get("prank_id", None)  # noqa

        try:
            prank = models.Prank.objects.get(id=prank_id)
        except models.Prank.DoesNotExist:
            raise exceptions.JokeDoesNotExistsAPIException()

        prank.delete()

        return Response(
            data={"code_transaction": "OK", "message": "Broma eliminada Exitosamente."},
            status=status.HTTP_200_OK,
        )
