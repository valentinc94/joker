from django.db.transaction import atomic
from rest_framework import serializers

from joker.apps.jokes import constants, models
from joker.domains.jokes import processes
from joker.interfaces.api.jokes import exceptions


class PrankDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Prank
        fields = [
            "id",
            "provider",
            "provider_reference",
            "joke",
            "created_at",
            "modified_at",
        ]


class PrankSerializer(serializers.Serializer):
    provider = serializers.CharField(required=False, allow_blank=True, allow_null=True)
    provider_reference = serializers.CharField(required=False, allow_blank=True, allow_null=True)
    joke = serializers.CharField(required=False, default="")

    def create(self, validated_data):
        with atomic():
            provider = validated_data.pop("provider", constants.JokesSources.OTHER)
            provider_reference = validated_data.pop("provider_reference", "")

            try:
                return processes.generate_new_joke(
                    joke=validated_data["joke"],
                    provider=provider,
                    provider_reference=provider_reference,
                )
            except processes.JokeAlreadyExists:
                raise exceptions.JokeAlreadyExistsAPIException()

    def update(self, prank: models.Prank, validated_data):
        with atomic():
            joke = validated_data.get("joke", prank.joke)
            prank.update_joke(joke=joke)
            return prank
