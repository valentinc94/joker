from rest_framework import serializers


class ExceptionSerializer(serializers.Serializer):  # noqa
    code = serializers.ReadOnlyField(
        help_text="Unique error code. Doesn't contain whitespaces. Words are separated by underscores."
    )
    message = serializers.ReadOnlyField(help_text="Human-readable description of the error.")


class LowestCommonMultipleSerializer(serializers.Serializer):  # noqa
    code_transaction = serializers.ReadOnlyField(
        help_text="Unique error code. Doesn't contain whitespaces. Words are separated by underscores."
    )
    lowest_common_multiple = serializers.ReadOnlyField(help_text="Minimo Comun Multiplo.")


class LowestCommonMultipleSerializer(serializers.Serializer):  # noqa
    code_transaction = serializers.ReadOnlyField(
        help_text="Unique error code. Doesn't contain whitespaces. Words are separated by underscores."
    )
    lowest_common_multiple = serializers.ReadOnlyField(help_text="Minimo Comun Multiplo.")


class NextNumberSerializer(serializers.Serializer):  # noqa
    code_transaction = serializers.ReadOnlyField(
        help_text="Unique error code. Doesn't contain whitespaces. Words are separated by underscores."
    )
    next_number = serializers.ReadOnlyField(help_text="Numero Secuencia.")
