from rest_framework.exceptions import APIException


class BaseAPIException(APIException):
    def response(self):
        return {"code_transaction": self.default_code, "message": self.default_detail}


class UserUnauthorizedAPIException(BaseAPIException):
    status_code = 401
    default_detail = "No tiene autorizacion"
    default_code = "ERROR_AUTH"
