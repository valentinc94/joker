from django.urls import path

from joker.interfaces.api.maths import views

app_name = "maths_api"


urlpatterns = [
    path(
        "api/v1/maths/lowest_common_multiple/",
        views.LowestCommonMultiple.as_view(),
        name="get_lowest_common_multiple",
    ),
    path(
        "api/v1/maths/next_sequence/",
        views.NextNumber.as_view(),
        name="get_next_sequence",
    ),
]
