from joker.interfaces.api.commons import exceptions


class InvalidInputAPIException(exceptions.BaseAPIException):
    status_code = 400
    default_detail = "Lista invalida, porfavor ingresar una lista de Numeros enteros."
    default_code = "INVALID_INPUT_EXIST"


class InvalidNumberInputAPIException(exceptions.BaseAPIException):
    status_code = 400
    default_detail = "Numero invalido, porfavor ingresar un Numero entero."
    default_code = "INVALID_INPUT_EXIST"
