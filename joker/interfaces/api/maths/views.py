import ast

from drf_yasg2 import openapi
from drf_yasg2.utils import swagger_auto_schema
from rest_framework import status, views
from rest_framework.response import Response

from joker.domains.maths import operations
from joker.interfaces.api.commons import exceptions as commons_exceptions
from joker.interfaces.api.commons import serializers as commons_serializers
from joker.interfaces.api.maths import exceptions
from joker.utils import mixins


class LowestCommonMultiple(mixins.APIWithUserPermissionsMixin, views.APIView):
    @swagger_auto_schema(
        operation_description="Endpoint para Obtener minimo comun multiplo de una Lista de Numeros",
        responses={
            200: commons_serializers.LowestCommonMultipleSerializer(many=False),
            400: openapi.Response(
                type=openapi.TYPE_OBJECT,
                description="",
                schema=commons_serializers.ExceptionSerializer(many=False),
                examples={
                    "application/json": exceptions.InvalidInputAPIException().get_full_details()
                },
            ),
            401: openapi.Response(
                type=openapi.TYPE_OBJECT,
                description="",
                schema=commons_serializers.ExceptionSerializer(many=False),
                examples={
                    "application/json": commons_exceptions.UserUnauthorizedAPIException().get_full_details()
                },
            ),
        },
    )
    def get(self, request, *args, **kwargs):
        numbers = request.GET.get("numbers", None)

        if not numbers:
            raise exceptions.InvalidInputAPIException()

        try:
            numbers = ast.literal_eval(numbers)
        except ValueError:
            raise exceptions.InvalidInputAPIException()

        try:
            lowest_common_multiple = operations.get_lowest_common_multiple_from_list(
                numbers=numbers
            )
        except operations.InvalidInput:
            raise exceptions.InvalidInputAPIException()
        else:
            return Response(
                {
                    "code_transaction": "OK",
                    "lowest_common_multiple": lowest_common_multiple,
                },
                status=status.HTTP_200_OK,
            )


class NextNumber(mixins.APIWithUserPermissionsMixin, views.APIView):
    @swagger_auto_schema(
        operation_description="Endpoint para Obtener el numero consecutivo del numero de entrada",
        responses={
            200: commons_serializers.NextNumberSerializer(many=False),
            400: openapi.Response(
                type=openapi.TYPE_OBJECT,
                description="",
                schema=commons_serializers.ExceptionSerializer(many=False),
                examples={
                    "application/json": exceptions.InvalidNumberInputAPIException().get_full_details()
                },
            ),
            401: openapi.Response(
                type=openapi.TYPE_OBJECT,
                description="",
                schema=commons_serializers.ExceptionSerializer(many=False),
                examples={
                    "application/json": commons_exceptions.UserUnauthorizedAPIException().get_full_details()
                },
            ),
        },
    )
    def get(self, request, *args, **kwargs):
        number = request.GET.get("number", None)

        if not number:
            raise exceptions.InvalidNumberInputAPIException()

        try:
            next_number = operations.get_next_number(number=number)
        except operations.InvalidInput:
            raise exceptions.InvalidNumberInputAPIException()
        else:
            return Response(
                {
                    "code_transaction": "OK",
                    "next_number": next_number,
                },
                status=status.HTTP_200_OK,
            )
