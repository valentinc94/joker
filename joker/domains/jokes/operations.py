import requests
from django.conf import settings

from joker.apps.jokes import constants


class FailedObtainingJoke(Exception):
    pass


def get_info_from_url(url: str) -> dict:
    headers = {"Accept": "application/json"}
    response = requests.get(url, headers=headers)

    if response.status_code != 200:
        raise FailedObtainingJoke("Fallo obteniendo la broma")

    response_body = response.json()
    return response_body


def get_chuck_prank() -> dict:
    response_body = get_info_from_url(url=settings.JOKES_CHUCK_API_URL)
    return {
        "provider": constants.JokesSources.CHUCK_NORRIS,
        "provider_reference": response_body["id"],
        "joke": response_body["value"],
    }


def get_dad_prank() -> dict:
    response_body = get_info_from_url(url=settings.JOKES_DAD_API_URL)
    return {
        "provider": constants.JokesSources.DAD,
        "provider_reference": response_body["id"],
        "joke": response_body["joke"],
    }
