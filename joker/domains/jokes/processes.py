from joker.apps.jokes import constants, models
from joker.domains.jokes import operations, queries


class JokeAlreadyExists(Exception):
    pass


class ProviderDoesNotExist(Exception):
    pass


class NotMatchJokeProvider(Exception):
    pass


def generate_new_joke(
    joke: str,
    provider: constants.JokesSources = constants.JokesSources.OTHER,
    provider_reference: str = None,
) -> models.Prank:
    if queries.check_if_prank_exists(joke=joke):
        raise JokeAlreadyExists("La broma ya existe registrada en el sistema.")

    return models.Prank.new(joke=joke, provider=provider, provider_reference=provider_reference)


def generate_random_joke(provider: str = None) -> dict:
    if not provider:
        raise ProviderDoesNotExist()

    if provider == constants.JokesSources.CHUCK_NORRIS.value:
        return operations.get_chuck_prank()
    else:
        return operations.get_dad_prank()
