from joker.apps.jokes import models


def check_if_prank_exists(joke: str) -> bool:
    return models.Prank.objects.filter(joke=joke).exists()
