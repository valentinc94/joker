from functools import reduce
from math import gcd

from joker.domains.maths import queries


class InvalidInput(Exception):
    pass


def get_lowest_common_multiple_from_list(numbers: list) -> int:
    if type(numbers) != list:
        raise InvalidInput()

    if not queries.check_if_is_a_valid_list_of_numbers(numbers=numbers):
        raise InvalidInput()

    return reduce(lambda a, b: a * b // gcd(a, b), numbers)


def get_next_number(number: str) -> int:
    if not queries.check_if_input_is_a_number(number=number):
        raise InvalidInput()

    return int(number) + 1
