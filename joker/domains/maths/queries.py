def check_if_is_a_valid_list_of_numbers(numbers: list) -> bool:
    return all([isinstance(number, int) for number in numbers])


def check_if_input_is_a_number(number: str) -> bool:
    return number.isnumeric()
