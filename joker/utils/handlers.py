import logging

from rest_framework import status
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.response import Response
from rest_framework.views import exception_handler

from joker.interfaces.api.commons import exceptions as commons_exceptions
from joker.interfaces.api.jokes import exceptions as jokes_exceptions
from joker.interfaces.api.maths import exceptions as maths_exceptions

logger = logging.getLogger(__name__)


EMPTY_FIELDS = "EMPTY_FIELDS"
ERROR_SERVER = "ERROR_SERVER"
USERNAME_EXISTS = "USERNAME_EXISTS"
ERROR_AUTH = "ERROR_AUTH"
NOT_EXIST = "NOT_EXIST"
INTERNAL_SERVER_ERROR = (
    "Oh Oh, error interno. Si este error persiste por favor comunicarse con servicio al cliente"
)


def joker_api_exception_handler(exc, context):
    response = exception_handler(exc, context)

    if response is not None:
        # Set response and notify monitoring
        if any(
            (
                type(exc) is commons_exceptions.UserUnauthorizedAPIException,
                type(exc) is jokes_exceptions.JokeAlreadyExistsAPIException,
                type(exc) is jokes_exceptions.JokeDoesNotExistsAPIException,
                type(exc) is jokes_exceptions.NotMatchJokeProviderAPIException,
                type(exc) is jokes_exceptions.UnableToGetJokeAPIException,
                type(exc) is maths_exceptions.InvalidInputAPIException,
                type(exc) is maths_exceptions.InvalidNumberInputAPIException,
            )
        ):
            logger.warning(exc)
            response.data = exc.response()

        elif type(exc) is MethodNotAllowed:
            logger.warning(exc)
            response.data = {"code_transaction": ERROR_SERVER, "message": response.data}

        elif response.status_code == 400:
            logger.warning(exc)
            response.data = {"code_transaction": EMPTY_FIELDS, "message": response.data}

        elif response.status_code == 401 or response.status_code == 403:
            logger.warning(exc)
            response.data = {"code_transaction": ERROR_AUTH, "message": response.data}

        elif response.status_code == 404:
            logger.warning(exc)
            response.data = {"code_transaction": NOT_EXIST, "message": "Objeto no existe"}

        else:
            logger.exception(exc)
            response.data = {"code_transaction": ERROR_SERVER, "message": response.data}
    else:
        logger.exception(exc)
        response = {
            "code_transaction": ERROR_SERVER,
            "message": INTERNAL_SERVER_ERROR,
        }
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    return response
