import factory

from joker.apps.jokes import constants, models


class PrankFactory(factory.django.DjangoModelFactory):
    provider_reference = factory.Faker("bothify", text="####")
    provider = constants.JokesSources.CHUCK_NORRIS
    joke = factory.Sequence(
        lambda n: "Quien vive en una piña debajo del mar? BOB ESPONJA{}".format(n)
    )

    class Meta:
        model = models.Prank
