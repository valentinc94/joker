import uuid

from django.db import models
from django_extensions.db import fields

from joker.apps.jokes import constants


class Prank(models.Model):
    uuid = models.UUIDField("UUID", unique=True, default=uuid.uuid4, editable=False, db_index=True)
    provider = models.CharField(
        max_length=350,
        verbose_name="Proveedor de Broma",
        choices=constants.JokesSources.choices,
        default=constants.JokesSources.OTHER,
    )
    provider_reference = models.CharField(
        max_length=350, verbose_name="ID en el proveedor de Bromas", blank=True
    )
    joke = models.TextField(verbose_name="Broma")

    created_at = fields.CreationDateTimeField(verbose_name="Fecha de Creación")
    modified_at = fields.ModificationDateTimeField(verbose_name="Ultimo Fecha de Actulizacion")

    @classmethod
    def new(
        cls,
        *,
        joke: str,
        provider: constants.JokesSources,
        provider_reference: str,
    ) -> "Prank":
        return cls.objects.create(
            provider=provider,
            provider_reference=provider_reference,
            joke=joke,
        )

    class Meta:
        verbose_name = "Broma"
        verbose_name_plural = "Bromas"
        ordering = ["-created_at"]

    def update_joke(self, joke: str):
        self.joke = joke
        self.save()
