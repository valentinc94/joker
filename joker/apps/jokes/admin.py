from django.conf import settings
from django.contrib import admin

from joker.apps.jokes import models


@admin.register(models.Prank)
class PrankAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "provider",
        "provider_reference",
        "get_joke",
        "created_at",
        "modified_at",
    )
    icon_name = "perm_contact_calendar"
    date_hierarchy = "created_at"
    list_per_page = settings.NUMBER_PAGINATION_ADMIN
    list_filter = ("provider",)
    search_fields = (
        "uuid",
        "provider_reference",
    )

    def get_joke(self, obj):
        joke_initial = obj.joke[:30]
        return f"{joke_initial}..."

    get_joke.short_description = "Inicio de Broma"
