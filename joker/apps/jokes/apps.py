from django.apps import AppConfig


class BenefitsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "joker.apps.jokes"
    verbose_name = "Bromas"
