from django.db import models


class JokesSources(models.TextChoices):
    CHUCK_NORRIS = "CHUCK_NORRIS", "Chuck Norris"
    DAD = "DAD", "Dad"
    OTHER = "OTHER", "Otro"


ACTIVE_JOKES_PROVIDERS = ["CHUCK_NORRIS", "DAD"]
